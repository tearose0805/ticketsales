import { Injectable } from '@angular/core';
import {IUser} from "../../models/users";

//в сервисе хранится информация об авторизованном пользователе
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private user: IUser | null;
  private token: string;
  private storeToken: string;

  constructor() {
  }

  //возвращается  авторизованного user
  getUser(): IUser|null {
    return this.user;
  }

  //записывается авторизованного пользователя в this.user
  setUser(user: IUser): void {
    this.user = user;
  }
  setToken(token:string): void{
    this.token = token;
    window.localStorage.setItem('userToken', token);
}
  getToken ():string{
    if(this.token) {
      return this.token;
    }else {
      const isTokeninStorage = window.localStorage.getItem('userToken');
      if(isTokeninStorage){
        return isTokeninStorage;
      }
    }
    return 'no token';
  }

  clearUser():void{
    this.user = null;
    this.token = '';
  }

}
