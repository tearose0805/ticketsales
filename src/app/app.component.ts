import { Component } from '@angular/core';
import {ObservableExampleService} from "./services/testing/observable-example.service";
import {ConfigService} from "./services/config/config.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ticketSales2022';

  // prop:string;
  constructor(private testing: ObservableExampleService,
              private configService: ConfigService) {
    testing.initObservable();
  }

  ngOnInit() {

    //Observable

//first subscriber
    const myObservable = this.testing.getObservable();
    myObservable.subscribe((data) => {
      //console.log('first Observable data: ', data);
    });

    //second subscriber
    myObservable.subscribe((data) => {
      //console.log('second Observable data: ', data);
    });

    //Subject
    const mySubject = this.testing.getSubject();

    /*    mySubject.subscribe((data)=>{
        //  console.log('first subject data: ', data);
          });
        mySubject.subscribe((data)=>{
        //  console.log('second subject data: ', data);
        });*/

    //send subject data
    mySubject.next('subject value 1');
    mySubject.next('subject value 2');


    //BehaviorSubject
    const myBehavior = this.testing.getBehaviorSubject();
    myBehavior.subscribe((data) => {
      // console.log('first behaviorSubject data: ', data);
    });
    myBehavior.next('new data from behaviorSubject');
    myBehavior.next('new data1 from behaviorSubject');

    this.configService.configLoad();
  }

}
