import { Component, OnInit } from '@angular/core';
import {StatisticService} from "../../../services/statistic/statistic.service";
import {ICustomStatisticUser} from "../../../models/users";

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.scss']
})
export class StatisticComponent implements OnInit {
 //определение колонок для вывода данных в таблице статистики
  cols = [
    { field: 'name', header: 'Имя' },
    { field: 'company', header: 'Компания' },
    { field: 'phone', header: 'Телефон' },
    { field: 'city', header: 'Город' },
    { field: 'street', header: 'Улица' }
  ];

  users:ICustomStatisticUser[];

  constructor(private staticService:StatisticService) { }

  ngOnInit(): void {
//получили данные по статистике с сервера при помощи подписки
//записали массив данных в свойство класса
    this.staticService.getUserStatistic().subscribe((data)=>{
    this.users = data;
    console.log('данные с сервера');
    });
 }

}
