import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TicketsRoutingModule } from './tickets-routing.module';
import { TicketsComponent } from './tickets.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TicketListComponent } from './ticket-list/ticket-list.component';
import { AsideComponent } from './aside/aside.component';

//добавили модуль из библиотеки ng Prime для меню
import {MenubarModule} from "primeng/menubar";

//добавили модуль из библиотеки ng Prime для выпадающего меню
import {DropdownModule} from 'primeng/dropdown';

//добавили модуль для использования директивы ngModel
import {FormsModule} from '@angular/forms'

//импортировали созданную директиву
import {BlocksStyleDirective} from "../../directive/blocks-style.directive";
import {InputTextModule} from "primeng/inputtext";
import {CalendarModule} from "primeng/calendar";
import {ToastModule} from "primeng/toast";
import {MessageService} from "primeng/api";


@NgModule({
  declarations: [
    TicketsComponent,
    HeaderComponent,
    FooterComponent,
    TicketListComponent,
    AsideComponent,
    BlocksStyleDirective
  ],
  imports: [
    CommonModule,
    TicketsRoutingModule,
    MenubarModule,
    DropdownModule,
    FormsModule,
    InputTextModule,
    CalendarModule,
    ToastModule,
    ],
  providers:[MessageService]
   })
export class TicketsModule { }
