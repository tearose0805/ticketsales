export interface ITour{
  name:string,
  description: string,
  tourOperator: string,
  price:string,
  img:string,
  id:string,
  type: string,
  date: string
}

export type TourType = 'Одиночный'|'Групповой';

//3.1) создали интерфейс
export interface ITourTypeSelect{
  label?: string,
  value?: string
  date?: string
}
//описывает ответ от endpoint nearest tour
export interface INearestTour extends ITour{
  locationId: string,
  country?:string|undefined
}

export interface ITourLocation{
  name: string;
  id: string
}

export interface ITourCountry{
  id: string;
  name: string|undefined
}
