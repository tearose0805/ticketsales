import { Component, OnInit } from '@angular/core';
import {MessageService} from 'primeng/api';
import {IUser} from "../../../models/users";
import {AuthService} from "../../../services/auth/auth.service";
import {ConfigService} from "../../../services/config/config.service";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {ServerError} from "../../../models/error";

function initializeApp(config: ConfigService) {
  return () => config.loadPromise().then(() => {
    console.log('---CONFIG LOADED--', ConfigService.config)
  });
}
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  login: string;
  psw: string;
  pswRepeat: string;
  email: string;
  cardNumber:string;
  selectedValue: boolean;
  storedUser: string ="user";

  //свойство для хранения значения поля useUserCard
  //конфигурационного файла config.json
  showCardNumber: boolean;

  constructor(private messageService:MessageService,
              private authService: AuthService,
              private http: HttpClient) { }

  ngOnInit(): void {
    this.showCardNumber = ConfigService.config.useUserCard;
    console.log('showCardNumber ', this.showCardNumber);
  }

  registration(ev:Event): void|boolean{
     if(this.psw!==this.pswRepeat){
      this.messageService.add({severity:'error', summary:'Пароли не совпадают'});
      return false;
    }
    const userObj: IUser = {
      psw : this.psw,
      cardNumber: this.cardNumber,
      login: this.login,
      email: this.email
    }

     //тестовый объект
     const userNest={
       name: 'test',
       age:30
     }

    /*  if(!this.authService.isUserExists(userObj)){
      this.authService.setUser(userObj);*/

      this.http.post<IUser>('http://localhost:3000/users/', userObj).subscribe((data) =>{
        if(this.selectedValue && userObj?.login){
          const jsonUser = JSON.stringify(userObj);
          localStorage.setItem(this.storedUser +"_"+ userObj?.login, jsonUser);
        }
        this.messageService.add({severity:'success', summary:'Регистрация прошла успешно'});
        },
        (err:HttpErrorResponse)=>{
        console.log('err',err);
        const serverError = <ServerError>err.error;
        this.messageService.add({severity:'warn', summary:'Пользователь уже зарегистрирован'});
        })
 }

  storeUser(){
    if(this.selectedValue){
      console.log("Store user");

    }else{
      console.log("Don't store user");
    }
  }

  }
