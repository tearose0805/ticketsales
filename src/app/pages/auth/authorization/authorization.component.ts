import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {AuthService} from "../../../services/auth/auth.service";
import {IUser} from "../../../models/users";
import {MessageService} from "primeng/api";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../../services/user/user.service";
import {InputSwitchOnChangeEvent} from "primeng/inputswitch";
import {ConfigService} from "../../../services/config/config.service";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {ServerError} from "../../../models/error";

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})

export class AuthorizationComponent implements OnInit, OnDestroy, OnChanges {

  loginText = 'Логин';
  pswText = 'Пароль';
  psw: string;
  login: string;
  selectedValue: boolean;
  cardNumber: string;
  authTextButton: string;
  isDisabled: boolean;

  //в конструкторе создали поле для класса authService
  //типа класса AuthService, взятого из сервиса
  // причем экземпляр этого класса будет записан
  //в переменную authService
  constructor(private authService:AuthService,
              private messageService: MessageService,
              //инжектирование класса Router
              private router: Router,
              private route: ActivatedRoute,

              //инжектировали сервис UserService из раздела services/user
              private userService:UserService,
              private http:HttpClient) { }


  ngOnInit(): void {
   // console.log('init'); //для контроля работы метода при изменении состояния checkbox
    this.authTextButton = "Авторизоваться";
    this.isDisabled = ConfigService.config.disabledCheckbox;
    }

    //вызывается при уничтожении элемента
  ngOnDestroy():void {
    console.log('destroy');
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', changes);
      }

  vipStatusSelected(ev:InputSwitchOnChangeEvent):void{
    console.log('vip checkbox event: ',ev.checked);
     }

    //этот метод вызывается при клике на кнопке авторизация
    onAuth(ev:Event):void{
   // console.log('ev',ev); //для контроля вывода объекта события клика

      const authUser:IUser ={
      psw:this.psw,
      login: this.login,
      cardNumber: this.cardNumber
    };
      this.http.post<IUser>('http://localhost:3000/users/'+authUser.login, authUser).subscribe((data: IUser) => {
        this.userService.setUser(authUser);
        const token: string = 'user-private-token'+data.id;
        this.userService.setToken(token);
        this.router.navigate(['tickets/tickets-list']);
      },
        (err)=> {
          const serverError = <ServerError>err.error;
          this.messageService.add({severity: 'warn', summary: serverError.errorText});
        });
    }

}
    //вызываем метод проверки user на экземпляре класса
      //если пользователь есть выводим в консоль true
   /* if(this.authService.checkUser(authUser)){
      console.log('auth true');

      //записываем объект авторизованного пользователя в сервис
      this.userService.setUser(authUser);

      //здесь передается random token
      //на самом деле нужно передавать token
      //который получили с сервера
      this.userService.setToken('user-private-token');

      //переход на модуль tickets при успешной авторизации
      this.router.navigate(['tickets/tickets-list']);

    }else{
      console.log('auth false');
      this.messageService.add({severity:'error', summary:'Неверные данные'});

    }*/


