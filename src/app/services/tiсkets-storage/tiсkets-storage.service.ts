import { Injectable } from '@angular/core';
import {ITour} from "../../models/tours";

@Injectable({
  providedIn: 'root'
})
export class TicketsStorageService {
  private ticketStorage: ITour[]

  constructor() { }

  // запись данных в this.ticketStorage
  setStorage(data: ITour[]): void{
    this.ticketStorage = data;
  }
  // возвращает в this.ticketStorage
  getStorage(): ITour[]{
    return this.ticketStorage;
  }
}
