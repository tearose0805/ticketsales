import { Injectable } from '@angular/core';
import {IUser} from "../../models/users";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userStorage: IUser[] = [];
  storedUser: string ="user";

  constructor() { }


  checkUser(user:IUser): boolean{

    // обращаемся к массиву пользователей
    // применяем метод find
    //ищем элемент - объект
    //у которого значение поля login равно значению поля login
    // объекта, который получен на вход
    // при заполнении данных в форме пользователем
    const isUserInStorage = this.userStorage.find((el) =>el.login === user.login);
    console.log('пользователь в userStorage: ', isUserInStorage);
    let userInStore: IUser = <IUser>{} ;
    let isUserSavedInStore = localStorage.getItem(this.storedUser + "_"+ user?.login)
    console.log('пользователь в local storage: ', isUserSavedInStore);

   //если есть объект в localStorage
    //преобразуем его в объект JS
    if(isUserSavedInStore) {
      userInStore = JSON.parse(isUserSavedInStore);
    }
    //если пользователь с таким логином найден в нашем массиве пользователей
    // то метод find вернет в переменную isUserExists найденный объект
    if(isUserInStorage){
       //проверяем свойство пароль найденного объекта с паролем,
      //который ввел пользователь
      // метод возвращает при проверке true / false
      return isUserInStorage.psw === user.psw;

     } else if(userInStore.login === user.login){
        return userInStore.psw === user.psw;
    }
      //если мы не попадаем в блок if, т.е. объект с заданным логином
    //не найден в массиве объектов - users
    return false;
    }

  //метод будет создавать нового пользователя
  setUser(user:IUser):void{

    //прежде чем создавать нового пользователя
    // нужно проверить, что пользователя с таким логином нет в нашем массиве пользователей
    const isUserInStorage = this.userStorage.find((el) =>el.login === user.login);

    //добавлять нового пользователя в хранилище
    // только если пользователя в массиве нет
    //также проверяем, что есть user, и этот user является объектом
    if(!isUserInStorage && user?.login) {
      this.userStorage.push(user);
      console.log(user); //для контроля
    }
    }

  isUserExists(user:IUser): boolean {
     const isUserExists = this.userStorage.find((el) =>el.login === user.login);
     return !!isUserExists;
  }

  //метод для получения массива зарегистрированных пользователей из приложения
  getRegisteredUsers():IUser[]{
    return this.userStorage;
  }
  getRegisteredUserObj(user:IUser):IUser{
    const userObj = this.userStorage.find((el) =>el.login === user.login);
    return userObj!;
  }
  }
