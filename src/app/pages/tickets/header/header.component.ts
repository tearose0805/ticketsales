import {Component, Input, OnDestroy, OnInit, SimpleChanges, OnChanges} from '@angular/core';

//класс menuItem из библиотеки ng Prime
import {MenuItem} from "primeng/api";
import {IUser} from "../../../models/users";
import {UserService} from "../../../services/user/user.service";
import {IMenuType} from "../../../models/menuType";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy, OnChanges {
  @Input()
  menuType: IMenuType;
  items: MenuItem[];
  private settingsActive = false;

  //свойства для таймера
  time:Date;
  private timerInterval:number;

  //свойство для авторизованного пользователя
  user:IUser|null;

     //инжектировали сервис UserService из раздела services/user
  constructor(private userService:UserService) {
      }

  //определили элементы меню из библиотеки ng Prime
    ngOnInit(): void {
    this.items = this.initMenuItems();

    //установка интервала, каждую секунду будет создаваться объект Date
    //таким образом, время будет изменятся каждую секунду
    // так работает таймер
    this.timerInterval = window.setInterval(()=>{
      this.time = new Date();
      console.log('run'); //для контроля интервалов
    },1000);

    //записали авторизованного user в свойство класса
    this.user = this.userService.getUser();
    console.log(this.user?.login)
      }

      //очистка интервала
      ngOnDestroy():void{
    if(this.timerInterval ){
      window.clearInterval(this.timerInterval);
    }
    }

  initMenuItems(): MenuItem[] {
    return [
      {
        label: 'Билеты',
        routerLink:['tickets-list']
      },
      {
        label: 'Настройки',
        routerLink:['./settings'],
        visible: this.settingsActive
      },
      {
        label: 'Выйти',
        routerLink:['/auth'],
        command:()=>{
          this.userService.clearUser();
        }
      },
   ];
  }

  ngOnChanges(ev: SimpleChanges): void {
    console.log('simple changes: ', ev);
    if(ev['menuType']){
      this.settingsActive = this.menuType?.type === "extended";
      this.items = this.initMenuItems();
    }

  }

  }
