import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from "@angular/forms";

import { AuthRoutingModule } from './auth-routing.module';

import { AuthComponent } from './auth.component'
import { AuthorizationComponent } from './authorization/authorization.component';
import { RegistrationComponent } from './registration/registration.component';

import { TabViewModule } from 'primeng/tabview';
import { InputTextModule} from 'primeng/inputtext';
//по инструкции библиотеки ng Prime для использования всплывающего сообщения
import {ToastModule} from 'primeng/toast';
import {CheckboxModule} from 'primeng/checkbox';
//по инструкции библиотеки ng Prime для вывода всплывающего сообщения об ошибке
import {MessageService} from 'primeng/api';


@NgModule({
  declarations: [
    AuthorizationComponent,
    AuthComponent,
    RegistrationComponent,

  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    TabViewModule,
    InputTextModule,
    FormsModule,
    CheckboxModule,
    ToastModule
  ],
  providers: [MessageService]
})
export class AuthModule { }
