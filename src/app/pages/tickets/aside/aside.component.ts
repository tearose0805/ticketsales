import {AfterViewInit, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {IMenuType} from "../../../models/menuType";
import {ITourTypeSelect} from "../../../models/tours";
import {TicketService} from "../../../services/tickets/ticket.service";
import {MessageService} from "primeng/api";
import {SettingsService} from "../../../services/settings/settings.service";

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss']
})
export class AsideComponent implements OnInit, AfterViewInit {
  menuTypes: IMenuType[];
  selectedMenuType: IMenuType;
  tourDefaultDate = new Date;

  //4.2) создали свойство
  tourTypes: ITourTypeSelect[] = [
    {label: 'Все', value: 'all'},
    {label: 'Одиночный', value: 'single'},
    {label: 'Групповой', value: 'multi'}
  ]

  @Output()
  updateMenuType: EventEmitter<IMenuType> = new EventEmitter();

  constructor(private ticketService:TicketService,
              private messageService: MessageService,
              private settingsService: SettingsService)
              { }

  ngOnInit(): void {
    this.menuTypes = [
      {type: 'custom', label : 'Обычное'},
      {type: 'extended', label : 'Расширенное'}
    ]
  }
  changeType(ev: {ev: Event, value: IMenuType}): void {
     this.updateMenuType.emit(ev.value);
      }

      ngAfterViewInit():void{
    //this.ticketService.updateTour({date: this.tourDefaultDate.toString()});

      }

  //4.2) создали метод changeTourType
  changeTourType(ev:  {ev: Event, value: ITourTypeSelect}): void {
     this.ticketService.updateTour(ev.value);
      }

  //7.4) добавили метод
  selectDate(ev: string) {
    console.log('xx', ev)
    this.ticketService.updateTour({date:ev})
  }

  initRestError(): void {
    this.ticketService.getError().subscribe({
        next:(data)=> {
          //console.log('ошибка');
        },
        error: (err) => {
          console.log('err', err)
          this.messageService.add({severity:'error', summary: 'Error', detail: 'Message Content'});
        },
      complete: () =>{}
      }
    );
  }

  initSettingsData():void{
    this.settingsService.loadUserSettingsSubject({
      saveToken: false
    });
  }

}
