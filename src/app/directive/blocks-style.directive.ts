import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';


@Directive({
  selector: '[appBlocksStyle]',
  host: {
    '(document:keyup)': 'initKeyUp($event)',
  },
  exportAs: 'blocksStyle'
  })
export class BlocksStyleDirective implements OnInit, AfterViewInit,OnChanges{


  //для указания селектора, по которому будет сделана выборка элементов
  @Input() selector: string;
  @Input() choice: boolean;

  //для выделения первого элемента - тура
  //если значение true, то первый элемент сразу должен быть выделен рамкой
  @Input() initFirst: boolean = true;

  @Output() renderComplete = new EventEmitter<boolean>();

  //массив элементов
  private items:HTMLElement[];
  private index: number = 0;
  public activeElementIndex: number = 0;
  private noBorderOnInitFirst:boolean;

  constructor(private el: ElementRef) { }

  ngOnInit():void{

  }

  ngAfterViewInit(){
     setTimeout(() => {
      this.renderComplete.emit(true);
      this.setInitialBorder();
    })
  }

  ngOnChanges(data: SimpleChanges) {
  }


initKeyUp(ev:KeyboardEvent):void{
    console.log('ev', ev); //для контроля свойств объекта ev

    //событие нажатие клавиши образуется всегда при нажатии на любую клавишу
    //поэтому следует добавить проверку перед удалением рамки с элемента
    if(ev.key ==='ArrowRight'|| ev.key === 'ArrowLeft'){

      //как только происходит событие, т.е. нажатие клавиши стрелки вправо- влево на каком- то элементе
      //нужно удаить границу на предыдущем выделенном элементе
      (this.items[this.index] as HTMLElement).removeAttribute('style');
    }

    //название - ключ клавиши вправо взято из свойств объекта события ev
    if(ev.key ==='ArrowRight'){
      if(this.noBorderOnInitFirst){
        (this.items[this.index] as HTMLElement).setAttribute('style', 'border: 2px solid red');
        this.noBorderOnInitFirst = false;
      }else{
        this.index++;
        if(this.items[this.index]){
          (this.items[this.index] as HTMLElement).setAttribute('style', 'border: 2px solid red');
        }else{
          this.index=this.items.length-1;
          (this.items[this.index] as HTMLElement).setAttribute('style', 'border: 2px solid red');
        }
      }
    } else if (ev.key === 'ArrowLeft') { //тот же алгоритм только при передвижении влево
      this.index--;
      if (this.items[this.index]) {
        (this.items[this.index] as HTMLElement).setAttribute('style', 'border: 2px solid red');
      }else{
        this.index=0;
        (this.items[this.index] as HTMLElement).setAttribute('style', 'border: 2px solid red');
       }
    }
    this.activeElementIndex = this.index;
  }

  initStyle(index:number){
    console.log(index);
    this.activeElementIndex = this.index;
    if(this.items){
      (this.items[index] as HTMLElement).setAttribute('style', 'border: 2px solid blue');
      this.index = index;
    }else {
      this.index = 0;
    //this.noBorderOnInitFirst = true;
      setTimeout(()=>{
        this.setInitialBorder();
      });
    }
  }

  //5.1) добавили новый метод
  updateItems(): void {
    this.items = this.el.nativeElement.querySelectorAll(this.selector);
    console.log ('получили массив элементов: ', this.items);
  }

  setInitialBorder():void{
    console.log('view');
    if(this.selector) {

     this.items = (this.el.nativeElement).querySelectorAll('div ticket-item');
       console.log('массив билетов' , this.items);

      if (!this.choice) {
        console.log('сделан выбор');

        if (this.initFirst) {
          console.log('помечать первый элемент');
          const firstEl = this.items[0] as HTMLElement;
          console.log('первый элемент ', firstEl);

          if (this.items[0]) {
            console.log('у нас есть первый элемент');

            //тогда этому первому элементу назначаем атрибут style, который определяет рамку выделения
            (this.items[0] as HTMLElement).setAttribute('style', 'border: 2px solid red');
            console.log('первый элемент ', this.items[0]);
          }
        } else {
          this.noBorderOnInitFirst = true;
          console.log('перешли в ветку else');
        }
      }
    }else {  //если селектор не передан, выводим ошибку
      console.error('Не передан селектор');
    }
  }

}
