import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TicketService} from "../../../services/tickets/ticket.service";
import {ITour, ITourTypeSelect} from "../../../models/tours";
import {TicketsStorageService} from "../../../services/tiсkets-storage/tiсkets-storage.service";
import {Router} from "@angular/router";
import {BlocksStyleDirective} from "../../../directive/blocks-style.directive";
import {debounceTime, fromEvent, Subscription} from "rxjs";

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent implements OnInit, AfterViewInit,OnDestroy {
  //свойство для хранения данных, полученных с сервера
  tickets:ITour[] =[];
  ticketsCopy:ITour[];
  ticketsFilter: ITour[];
  loadCountBlock = true;
  //defaultDate: string;
  directiveReady = false;
  noSearchResults:boolean = false;

   //3.1) создали приватное поле
  private tourUnsubscriber: Subscription;

  @ViewChild('tourWrap',{read: BlocksStyleDirective}) blockDirective: BlocksStyleDirective;

  @ViewChild('tourWrap') tourWrap: ElementRef;
  @ViewChild('ticketSearch') ticketSearch: ElementRef;

  searchTicketSub: Subscription;
  ticketSearchValue: string;

  constructor(private ticketService: TicketService,
              private router: Router,
              private ticketStorage:TicketsStorageService,
              ) { }

  ngOnInit(): void {
    this.ticketService.getTickets().subscribe(
      (data)=>{
        //записываем данные с сервера в переменную класса
        this.tickets = data;
        this.ticketsCopy = [...this.tickets];
        this.ticketsFilter = [...this.tickets];
        this.ticketStorage.setStorage(data);
      });
   this.tourUnsubscriber = this.ticketService.ticketType$.subscribe((data: ITourTypeSelect) => {
      console.log('data', data)

      //let ticketType: string;
      switch (data.value) {
        case "single":
          this.tickets = this.ticketsFilter.filter((el) => el.type === "single");
          break;
        case "multi":
          this.tickets = this.ticketsFilter.filter((el) => el.type === "multi");
          break;
        case "all":
          this.tickets = [...this.ticketsFilter];
          break;
      }
      if (data.date) {
        const dateWithoutTime = new Date(data.date).toISOString().split('T');
        const dateValue = dateWithoutTime[0]
        console.log('dateValue',dateValue)
        this.tickets = this.ticketsFilter.filter((el) => el.date === dateValue);
      }

      setTimeout(() => {
        this.blockDirective.updateItems();
        this.blockDirective.initStyle(0);  // сбрасываем индекс на 0 элемент
      });
    });

   }

  ngAfterViewInit(){
   console.log('native element ',this.ticketSearch.nativeElement);
  const fromEventObserver = fromEvent(this.ticketSearch.nativeElement, 'keyup');
    this.searchTicketSub = fromEventObserver.pipe(
     debounceTime(200)).subscribe((ev:any)=>{
        if(this.ticketSearchValue){
          this.tickets = this.ticketsCopy.filter((el:ITour)=>(el.name).toLowerCase().includes(this.ticketSearchValue.toLowerCase()));
          console.log('массив билетов ', this.tickets);
          if(!this.tickets.length){
            console.log('Нет результатов');
            this.noSearchResults = true;
          }
        }else {
          this.noSearchResults = false;
          this.tickets = [...this.ticketsCopy];
        }
      });
   }

  goToTicketInfoPage(item:ITour){
    this.router.navigate([`/tickets/ticket/${item.id}`])
  }

  directiveRenderComplete(ev:boolean){
    this.directiveReady = true;
    console.log('ev директива',ev);
    const el: HTMLElement = this.tourWrap.nativeElement;
    el.setAttribute('style', 'background-color: #9e9e9e38');
    if(ev){
    }
    }

  //4.3) метод подписки на изменение типа тура
  changeTourType():void{

  }

  //4.3) выполнили отписку
  ngOnDestroy() {
    this.tourUnsubscriber.unsubscribe();
  //  this.searchTicketSub.unsubscribe();
  }
}
