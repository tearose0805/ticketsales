//отображение информации о конкретном туре
import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {INearestTour, ITour, ITourCountry, ITourLocation} from "../../../models/tours";
import {ActivatedRoute} from "@angular/router";
import {TicketsStorageService} from "../../../services/tiсkets-storage/tiсkets-storage.service";
import {IUser} from "../../../models/users";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../../services/user/user.service";
import {TicketService} from "../../../services/tickets/ticket.service";
import {forkJoin, fromEvent, map, Subscription} from "rxjs";

@Component({
  selector: 'app-ticket-item',
  templateUrl: './ticket-item.component.html',
  styleUrls: ['./ticket-item.component.scss']
})
export class TicketItemComponent implements OnInit,AfterViewInit,OnDestroy {
  ticket: ITour | undefined;
  user: IUser | null;
  userForm: FormGroup;
  ticketSearchValue: string;
  nearestTours: INearestTour[];
  toursLocation: ITourLocation[];
  @ViewChild('ticketSearch') ticketSearch: ElementRef;
  searchTicketSub: Subscription;
  ticketRestSub: Subscription;
  searchTypes = [1,2,3];
  tourCountry: INearestTour[];

  constructor(private route: ActivatedRoute,
              private ticketStorage: TicketsStorageService,
              private userService: UserService,
              private ticketService: TicketService) {
  }

  ngOnInit(): void {

    //получаем авторизованного пользователя
    this.user = this.userService.getUser()

    //init FormGroup
    this.userForm = new FormGroup<any>({
      firstName: new FormControl('', {validators: Validators.required}),
      lastName: new FormControl('', [Validators.required, Validators.minLength(2)]),
      cardNumber: new FormControl(''),
      birthday: new FormControl(''),
      age: new FormControl(),
      citizen: new FormControl('')
    });

    //получить ближашие туры (объединяет в один массив два ответа от сервера
    //от двух endpoint)
    forkJoin([this.ticketService.getNearestTours(),
      this.ticketService.getToursLocation()]).
      subscribe((data) => {
      console.log('data from forkJoin: ', data); //контроль полученного ответа
      this.nearestTours = data[0];
      this.toursLocation = data[1];
      this.ticketService.nearestTours = data[0];
      this.ticketService.tourslocation = data[1];
      this.tourCountry = this.ticketService.getLocation(this.nearestTours, this.toursLocation);
      this.nearestTours = this.tourCountry;
      console.log(this.tourCountry);
      });

    // console.log(this.ticketService.getLocation());

    //используем специальное свойство snapshot класса ActivatedRoute
    //которое позволяет получить текущее состояние машрута
    //метод paramMap.get по ключу позволяет получить его значение
    // т.е. получаем id, который передан в адресной строке
    const routeIdParam = this.route.snapshot.paramMap.get('id');

    //также считываем параметр id только другиv способом
    const queryIdParam = this.route.snapshot.queryParamMap.get('id');

    //записываем считванный id одним или другим способом в переменную
    const paramValueId = routeIdParam || queryIdParam;

    //если параметр id считан
    if (paramValueId) {

      //в переменную ticketStorage записываем массив всех билетов
      //который хранится в сервисе TicketsStorageService
      //для этого получаем этот массив из сервиса при помощи метода getStorage
      const ticketStorage = this.ticketStorage.getStorage();

      //вызываем на массиве метод find, который возвращает объект - билет
      //id которого равно считанному id
      //в свойство класса ticket записываем найденный объект
      this.ticket = ticketStorage.find((el) => el.id === paramValueId);
      console.log('this.ticket', this.ticket)
    }
  }

  ngAfterViewInit() {

    //set card number
    this.userForm.controls["cardNumber"].setValue(this.user?.cardNumber);

    const fromEventObserver = fromEvent(this.ticketSearch.nativeElement, 'keyup');
    this.searchTicketSub = fromEventObserver.subscribe((ev:any)=>{
    this.initSearchTour();
    })
     }

     ngOnDestroy(){
    this.searchTicketSub.unsubscribe();

     }

   initSearchTour():void{
    if(this.ticketSearchValue){
      const type = Math.floor(Math.random()* this.searchTypes.length);

      if(this.ticketRestSub && !this.searchTicketSub.closed){
        this.ticketRestSub.unsubscribe();
      }

      this.ticketRestSub = this.ticketService.getRandomNearestEvent(type).subscribe ( (data:any)=>{
        console.log('нашли один тур ', data);
        this.nearestTours = this.ticketService.getLocation([data], this.toursLocation)
      });
    } else{
      this.nearestTours = this.tourCountry;
    }


   /* if (this.ticketSearchValue===''){

    }*/
     }

  onSubmit(): void {

  }

  initTour():void{
    const userData = this.userForm.getRawValue();
   //сохранили информацию о туре и все данные user из полей формы при помощи метода getRawValue
   //оба объекта: тур и личная информация объединены в один объект
    const postData = {...this.ticket,...userData};
    console.log('postData ', postData);

    //вывели данные из всех полей формы
    console.log(' this.userForm.getRawValue()', this.userForm.getRawValue());

    //инициализируем отправку данных на сервер при помощи subscribe
    this.ticketService.sendTourData(postData).subscribe();
  }
  selectDate(ev: Event): void {

  }

 }
