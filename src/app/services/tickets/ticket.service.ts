import { Injectable } from '@angular/core';
import {TicketRestService} from "../rest/ticket-rest.service";
import {map, Observable, Subject} from "rxjs";
import {INearestTour, ITour, ITourCountry, ITourLocation, ITourTypeSelect} from "../../models/tours";

@Injectable({
  providedIn: 'root'
})
export class TicketService {

 private ticketSubject = new Subject<ITourTypeSelect>()
 nearestTours: INearestTour[];
 tourslocation: ITourLocation[];


  readonly ticketType$ = this.ticketSubject.asObservable();

  constructor(private ticketServiceRest: TicketRestService) { }

   getTickets():Observable<ITour[]>{
    return this.ticketServiceRest.getTickets();
  }

  getNewTickets():Observable<ITour[]>{
    return this.ticketServiceRest.getTickets().pipe(map(
      (value)=>{
        const singleTour = value.filter((el)=> el.type === 'single');
        return value.concat(singleTour);
      }
      ));

  }

  getTicketTypeObservable(): Observable<ITourTypeSelect> {
    return this.ticketSubject.asObservable();
  }

  updateTour(type:ITourTypeSelect): void {
    this.ticketSubject.next(type);
  }

  getError() {
    return this.ticketServiceRest.getRestError();
  }

  //получаем observable с массивом ближайших туров
  //вызываем метод запроса на сервер
  getNearestTours():Observable<INearestTour[]>{
    return this.ticketServiceRest.getNearestTickets();
  }

  //вызываем метод запроса на сервер
  getToursLocation():Observable<ITourLocation[]>{
    return this.ticketServiceRest.getLocationList();
  }
  getLocation(arr1:INearestTour[],arr2:ITourLocation[]){
  const arr3= arr1.map((tour)=>{
  const locationId = tour.locationId;
  const getToutObj=arr2.find((tourinf)=>tourinf.id===locationId);
  tour.country = getToutObj?.name;
  return tour;
  });
  return arr3;
  }

  getRandomNearestEvent(type:number):Observable<INearestTour>{
    return this.ticketServiceRest.getRandomNearestEvent(type);
  }

  //метод лтправляет данные - объединенный объект тур + пользователь на сервер
  //где data - эта структура данных, которую отправляем на сервер
  sendTourData (data: any): Observable<any>{
    return this.ticketServiceRest.sendTourData(data);
  }

}
