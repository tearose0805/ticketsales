import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ObservableExampleService {

  private mySubject = new Subject<string>();

  private myObservable = new Observable<string>(subsriber =>{
    subsriber.next('sync some value');
    setTimeout(()=>{
      subsriber.next('some value');
    },3000);
  });

  private myBehaviorSubject = new BehaviorSubject<string>('some data of BehaviorSubject');

  constructor() { }

  initObservable():void{
    const observable = new Observable(subscriber => {
      subscriber.next(4);
      subscriber.next(5);
      setTimeout(()=>{
        subscriber.next('asyncData');
        subscriber.error('some error here');
      },3000);
    });

    const sub = observable.subscribe((data) =>{
        console.log('observable data: ', data);
      },
      (err) =>{
        console.log('error: ', err);
      });
    sub.unsubscribe();
  }
  getObservable():Observable<string>{
    return this.myObservable;
  }

  getSubject():Subject<string>{
    return this.mySubject;
  }

  getBehaviorSubject(): BehaviorSubject<string>{
    return this.myBehaviorSubject;
  }
}

