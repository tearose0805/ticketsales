import {Component, OnDestroy, OnInit} from '@angular/core';
import {ObservableExampleService} from "../../../services/testing/observable-example.service";
import {Subject, Subscription, take, takeUntil} from "rxjs";
import {SettingsService} from "../../../services/settings/settings.service";
import {IChangePsw, IUser} from "../../../models/users";
import {UserService} from "../../../services/user/user.service";
import {MessageService} from "primeng/api";
import {AuthService} from "../../../services/auth/auth.service";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
  //login: string;
  currentPsw: string;
  pswRepeat: string;
  newPsw: string;
  //cardNumber: string;
  selectedValue: boolean;

  //объект авторизованного пользователя из приложения
  storedUserInApp: IUser|null;

  //объект зарегистрированного пользователя из localStorage
  //если сохранен туда при регистрации
  //тип string, так как в localStorage формат данных json
  storedUserInLocalstorage: IUser|null;

  //массив зарегистрированных пользователей из приложения
  //может не нужен!
  userStorage:IUser[];

  currentPswCheck: boolean;

  //1.2) создали приватное поле типа Subject
  private subjectScope: Subject<string>;
  settingsData: Subscription;
  settingsDataSubject: Subscription;

   //2.1) создали приватное поле subjectUnsubscribe
   private  subjectUnsubscribe: Subscription;
  //1.1) инжектировали ObservableExampleService

  subjectForUnsibscribe = new Subject();

  constructor(private myObservable: ObservableExampleService,
              private settingsService: SettingsService,
              private userService:UserService,
              private messageService: MessageService,
              private authService: AuthService) {
  }

  ngOnInit(): void {
   //settingsData Observable
    this.settingsData = this.settingsService.loadUserSettings().pipe(takeUntil(this.subjectForUnsibscribe)).subscribe((data) => {
      console.log('settings data', data);
    });

//settingsData Subject
   this.settingsDataSubject = this.settingsService.getSettingsSubjectObservable().
   pipe(takeUntil(this.subjectForUnsibscribe)).
      subscribe((data)=>{
        console.log('settings data from subject ', data);
      })
    }
   changePsw(ev:Event):void {
     //получили объект из view от полей ввода (ngModel)
     const userNewPsw: IChangePsw = {
       currentPsw: this.currentPsw,
       newPsw: this.newPsw,
       pswRepeat: this.pswRepeat
     };

     //получили объект авторизованного пользователя из приложения
     //он хранится в UserService в свойстве User
     //при помощи метода сервиса, который возвращает этот объект
     //этот объект доступен только пока работает приложение
     //при перезагрузке приложения он уничтожается
     this.storedUserInApp = this.userService.getUser();
     console.log('получили  user из app: ', this.storedUserInApp);
     console.log('получили user из view: ', userNewPsw);


     //получили объект зарегистрированного пользователя из localStorage:
     const getObjFromLocalstorage = localStorage.getItem('user_' + this.storedUserInApp?.login);
     if (getObjFromLocalstorage) {

       //если объект пользователя в localStorage есть, преобразуем его в объект JS
       //сохраним объект в свойство класса
       this.storedUserInLocalstorage = JSON.parse(getObjFromLocalstorage);
       console.log('есть пользователь в local storage: ', this.storedUserInLocalstorage);
     }

     //этот блок выполняем, если есть объект авторизованного пользователя в UserService
     //если получен объект авторизованного пользователя из приложения:
     if (this.storedUserInApp) {

       //получаем объект пользователя из массива зарегистрированных пользователей
       //по логину авторизованного пользователя
       const registeredUserInAppObj = this.authService.getRegisteredUserObj(this.storedUserInApp);
       console.log('получили объект зарегистрированного пользователя из app: ', registeredUserInAppObj);

      //1) проверяем текущий пароль по массиву зарегистрированных пользователей
         if (registeredUserInAppObj) {
          this.currentPswCheck = this.settingsService.checkUserPsw(this.currentPsw, registeredUserInAppObj.psw);
         }else if (this.storedUserInLocalstorage) {
          this.currentPswCheck = this.settingsService.checkUserPsw(this.currentPsw, this.storedUserInLocalstorage!.psw);
         }

           //2) проверяем совпадение нового пароля и его повторения
            if(this.currentPswCheck) {
              if (this.settingsService.checkUserPsw(this.newPsw, this.pswRepeat)) {
                //проверка 1 и 2 прошли успешно, изменяем пароль пользователя в массиве объектов
                //зарегистрированных пользователей из приложения:
                //записываем туда новый пароль
                if(registeredUserInAppObj){
                  registeredUserInAppObj.psw = this.newPsw;
                }

               //проверяем, есть ли объект пользователя в localStorage по объекту JS
                // который получен из объекта localStorage:
                if (this.storedUserInLocalstorage) {

                  //удаляем этот объект пользователя из localStorage
                  localStorage.removeItem('user_' + this.storedUserInApp.login);

                  //меняем пароль в объекте JS из localStorage
                  this.storedUserInLocalstorage.psw = this.newPsw;

                  //преобразуем в строку обновленный объект пользователя (c новым паролем)
                  const jsonUser = JSON.stringify(this.storedUserInLocalstorage);

                  //записываем в localStorage этот обновленный объект пользователя
                  //пароль обновлен и хранилище в приложении, и в localStorage
                  localStorage.setItem("user_" + this.storedUserInApp.login, jsonUser);
                }

                console.log('пароль успешно изменен');
                this.messageService.add({severity: 'success', summary: 'Success', detail: 'Пароль успешно изменен'});

              } else {
                console.log('пароли не совпадают');
                this.messageService.add({severity: 'warn', summary: 'Warn', detail: 'Пароли не совпадают'});
              }
            }else {
              console.log('неверный пароль');
              this.messageService.add({severity: 'error', summary: 'Error', detail: 'Неверный пароль'});
            }

       //этот блок выаолняется, если нет объекта авторизованного пользователя в UserService
       //например, при перезагрузке страницы с настройками
       //когда вход не осуществляется через вкладку Авторизация
       //в этом случае пароль можно изменить по данным в localStorage
     } else {
       console.log('пользователь должен войти в систему');
       this.messageService.add({ severity: 'info', summary: 'Info', detail: 'Необходимо авторизоваться!' });
     }
  }

    ngOnDestroy():void {
      this.subjectForUnsibscribe.next(true);
      this.subjectForUnsibscribe.complete();
    }
  }
